﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SldWorks;
using SwConst;

// Selection types - http://help.solidworks.com/2012/english/api/swconst/SolidWorks.Interop.swconst~SolidWorks.Interop.swconst.swSelectType_e.html

namespace SolidworksMacros {
	class CurrentSelection {
		SldWorks.SldWorks swApp;
		ModelDoc2 swDoc;
		SelectionMgr swSelMgr;

		public List<Face2> faces = new List<Face2>();
		public List<Vertex> vertices = new List<Vertex>();
		public List<Edge> edges = new List<Edge>();
		public List<SketchPoint> sketchPoints = new List<SketchPoint>();
		public List<SketchSegment> sketchSegs = new List<SketchSegment>();
		public List<object> allObjects = new List<object>();

		public CurrentSelection(SldWorks.SldWorks swApp, ModelDoc2 swDoc) {
			this.swApp = swApp;
			this.swDoc = swDoc;
			swSelMgr = swDoc.SelectionManager;
			int numSelections = swSelMgr.GetSelectedObjectCount2(-1);
			for (int i = 1; i <= numSelections; i++) {
				swSelectType_e selType = (swSelectType_e)swSelMgr.GetSelectedObjectType3(i, -1);
				switch (selType) {
					case swSelectType_e.swSelFACES: faces.Add((Face2)swSelMgr.GetSelectedObject6(i, -1)); break;
					case swSelectType_e.swSelEDGES: edges.Add((Edge)swSelMgr.GetSelectedObject6(i, -1)); break;
					case swSelectType_e.swSelVERTICES: vertices.Add((Vertex)swSelMgr.GetSelectedObject6(i, -1)); break;
					case swSelectType_e.swSelSKETCHPOINTS: sketchPoints.Add((SketchPoint)swSelMgr.GetSelectedObject6(i, -1)); break;
					case swSelectType_e.swSelSKETCHSEGS: sketchSegs.Add((SketchSegment)swSelMgr.GetSelectedObject6(i, -1)); break;
				}
				allObjects.Add(swSelMgr.GetSelectedObject6(i, -1));
			}
		}
		public void clearSelection() {
			swDoc.ClearSelection2(true);
		}
		public void deselectObject(object thing) {
			if (allObjects.Contains(thing)) {
				int index = allObjects.IndexOf(thing);
				allObjects.Remove(thing);
				swSelMgr.DeSelect2(index + 1, -1);
			}
		}
		public void selectObject(object thing) {
			allObjects.Add(thing);
			swSelMgr.AddSelectionListObject(thing, swSelMgr.CreateSelectData());
		}
		public void mark(object thing, int mark) {
			int index = allObjects.IndexOf(thing);
			swSelMgr.SetSelectedObjectMark(index + 1, mark, (int)swSelectionMarkAction_e.swSelectionMarkSet);
		}
		public void resetSelection() {
			// Reselect everything to match the object array
			clearSelection();
			foreach (object thing in allObjects) {
				swSelMgr.AddSelectionListObject(thing, swSelMgr.CreateSelectData());
			}
		}
	}
}
