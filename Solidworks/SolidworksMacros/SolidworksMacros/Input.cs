﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

namespace SolidworksMacros {
	class Input {
		public static double getDouble(string msgText) {
			while (true) {
				string input = Interaction.InputBox("Circle diameter");
				if (input == "") throw new DoneException();
				if (double.TryParse(input, out double diameter)) return diameter;
			}
			
		}
	}
}
