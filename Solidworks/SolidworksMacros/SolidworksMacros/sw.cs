﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SldWorks;
using SwConst;

namespace SolidworksMacros {
	class SW {
		private static SW instance;
		public static SW sw { get { return instance; } }
		public static void setupSW() { instance = new SW(); }

		public ModelDoc2 swDoc;
		public SketchManager skMan;
		public Sketch sketch;
		public SketchRelationManager relMan;
		public SketchTools skTools;
		public SldWorks.SldWorks swApp;
		
		private SW() {
			swApp = new SldWorks.SldWorks();
			swDoc = swApp.ActiveDoc;
			skMan = swDoc.SketchManager;
			sketch = skMan.ActiveSketch;
			if (sketch is null) { System.Windows.Forms.MessageBox.Show("No sketch selected"); throw new DoneException(); }
			relMan = sketch.RelationManager;
			skTools = new SketchTools();
		}
	}
}
