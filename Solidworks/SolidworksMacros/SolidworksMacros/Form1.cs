﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SldWorks;
using SwConst;

namespace SolidworksMacros {
	public partial class Form1 : Form {
		public Form1() {
			InitializeComponent();
		}
		Main backend;
		private void Form1_Load(object sender, EventArgs e) {
			try { backend = new Main(); } catch (DoneException) { System.Windows.Forms.Application.Exit(); }
		}

		private void circlesButton_Click(object sender, EventArgs e) {
			try { backend.createIdenticalCircles(); } catch (DoneException) { }
		}

		private void createCenterlinesButton_Click(object sender, EventArgs e) {
			try { backend.createCenterlines(); } catch (DoneException) { }
		}

		private void centerVButton_Click(object sender, EventArgs e) {
			try { backend.centerOn(Direction.vertical); } catch (DoneException) { }
		}

		private void centerHButton_Click(object sender, EventArgs e) {
			try { backend.centerOn(Direction.horizontal); } catch (DoneException) { }
		}

		private void MirrorVButton_Click(object sender, EventArgs e) {
			try { backend.mirrorOn(Direction.vertical); } catch (DoneException) { }
		}

		private void MirrorHButton_Click(object sender, EventArgs e) {
			try { backend.mirrorOn(Direction.horizontal); } catch (DoneException) { }
		}
	}
	public class DoneException : Exception { }
}
