﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SldWorks;
using SwConst;
using System.Runtime.InteropServices;
using static SolidworksMacros.SW;

namespace SolidworksMacros {
	enum Direction {
		horizontal = 0,
		vertical = 1,
		other = 2
	}
	static class Misc {

		public static DispatchWrapper[] GetDispatchWrapperArray(object[] swObjects) {
			// Helps get solidworks functions to take arguments happily
			var dispWraps = new DispatchWrapper[swObjects.Length];
			for (int i = 0; i < swObjects.Length; i++)
				dispWraps[i] = new DispatchWrapper(swObjects[i]);
			return dispWraps;
		}

		static double eps = 0.0000001;
		public static bool same(double a, double b) {
			return Math.Abs(a - b) < eps;
		}
		public static bool same(SketchPoint a, SketchPoint b) {
			return same(a.X, b.X) && same(a.Y, b.Y) && same(a.Z, b.Z);
		}

		public static Direction direction(this SketchLine line) {
			if (same(line.Angle, 0) || same(line.Angle, Math.PI)) {
				return Direction.horizontal;
			}
			if (same(line.Angle, Math.PI * 0.5) || same(line.Angle, Math.PI * 1.5)) {
				return Direction.vertical;
			}
			return Direction.other;
		}

		public static SketchPoint midpoint(this SketchSegment skSeg) {
			if (skSeg is SketchLine) {
				SketchLine line = (SketchLine)skSeg;
				SketchPoint p1 = line.GetStartPoint2();
				SketchPoint p2 = line.GetEndPoint2();
				SketchPoint midpoint = sw.skTools.getPoint((p1.X + p2.X) / 2, (p1.Y + p2.Y) / 2, (p1.Z + p2.Z) / 2);
				object[] obj = new object[] { midpoint, line };
				sw.relMan.AddRelation(GetDispatchWrapperArray(obj), (int)swConstraintType_e.swConstraintType_ATMIDDLE);
				return midpoint;
			}
			if (skSeg is SketchArc) {
				SketchArc arc = (SketchArc)skSeg;
				return arc.GetCenterPoint2();
			}
			throw new ArgumentException("Only works with SketchLines, SketchArcs");
		}

	}
}
