﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SldWorks;
using SwConst;
using static SolidworksMacros.Misc;
using static SolidworksMacros.SW;

namespace SolidworksMacros {
	class SketchTools {
		public SketchTools() { }

		public double approxSketchSize() {
			double size = 0;
			foreach (SketchSegment seg in getSketchSegments()) {
				if (seg.GetLength() > size) size = seg.GetLength();
			}
			if (size == 0) return 0.01;
			return size;
		}

		private List<SketchSegment> getSketchSegments() {
			List<SketchSegment> segList = new List<SketchSegment>();
			object[] oSegs = sw.skMan.ActiveSketch.GetSketchSegments();
			foreach (object oSeg in oSegs ?? new object[] { }) {
				segList.Add((SketchSegment)oSeg);
			}
			return segList;
		}
		public List<SketchPoint> getSketchPoints() {
			List<SketchPoint> pointList = new List<SketchPoint>();
			object[] oPoints = sw.skMan.ActiveSketch.GetSketchPoints2();
			foreach (object oPoint in oPoints ?? new object[] { }) {
				pointList.Add((SketchPoint)oPoint);
			}
			return pointList;
		}

		public List<SketchLine> getSketchLines() {
			List<SketchLine> lineList = new List<SketchLine>();
			object[] oLines = sw.skMan.ActiveSketch.GetSketchSegments();
			foreach (object oLine in oLines ?? new object[] { }) {
				if (oLine is SketchLine) lineList.Add((SketchLine)oLine);
			}
			return lineList;
		}

		public SketchPoint getPoint(double X, double Y, double Z) {
			//Retrieve sketchpoint at coords, create it if not there
			foreach (SketchPoint point in getSketchPoints()) {
				if (same(point.X, X) && same(point.Y, Y) && same(point.Z, Z)) return point;
			}
			return sw.skMan.CreatePoint(X, Y, Z);
		}
		public SketchPoint getOrigin() { return getPoint(0, 0, 0); }

		

		public SketchLine getCenterLine(Direction dir) {
			//Retrieve horizontal or vertical centerline, create if not there
			//Must be line starting or ending at origin point, and be horisontal or vertical
			if (dir == Direction.other) throw new ArgumentException("Direction must be horizontal or vertical");
			SketchPoint centerPoint = getOrigin();
			foreach (SketchLine line in getSketchLines()) {
				SketchPoint point = (SketchPoint)line.GetStartPoint2();
				if (same(centerPoint, (SketchPoint)line.GetStartPoint2()) || same(centerPoint, (SketchPoint)line.GetEndPoint2())) {
					if (line.direction() == dir) return line;
				}
			}
			// No line was found, create one
			double length = approxSketchSize() * 0.8;
			SketchLine newLine;
			if (dir == Direction.horizontal) {
				newLine = (SketchLine)sw.skMan.CreateLine(0, 0, 0, length, 0, 0);
			} else {
				newLine = (SketchLine)sw.skMan.CreateLine(0, 0, 0, 0, length, 0);
			}
			((SketchSegment)newLine).ConstructionGeometry = true; 
			return newLine;
		}

		public void centerOnLine(SketchSegment skSeg, SketchLine centerLine) {
			SketchPoint midpoint = skSeg.midpoint();
			object[] obj = new object[] { centerLine, midpoint };
			sw.relMan.AddRelation(GetDispatchWrapperArray(obj), (int)swConstraintType_e.swConstraintType_COINCIDENT);
		}

	}
}
