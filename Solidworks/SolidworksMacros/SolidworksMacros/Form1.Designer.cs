﻿namespace SolidworksMacros {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.circlesButton = new System.Windows.Forms.Button();
			this.centerVButton = new System.Windows.Forms.Button();
			this.centerHButton = new System.Windows.Forms.Button();
			this.MirrorVButton = new System.Windows.Forms.Button();
			this.MirrorHButton = new System.Windows.Forms.Button();
			this.createCenterlinesButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// circlesButton
			// 
			this.circlesButton.Location = new System.Drawing.Point(12, 12);
			this.circlesButton.Name = "circlesButton";
			this.circlesButton.Size = new System.Drawing.Size(134, 23);
			this.circlesButton.TabIndex = 0;
			this.circlesButton.Text = "Make Identical Circles";
			this.circlesButton.UseVisualStyleBackColor = true;
			this.circlesButton.Click += new System.EventHandler(this.circlesButton_Click);
			// 
			// centerVButton
			// 
			this.centerVButton.Location = new System.Drawing.Point(152, 12);
			this.centerVButton.Name = "centerVButton";
			this.centerVButton.Size = new System.Drawing.Size(168, 23);
			this.centerVButton.TabIndex = 1;
			this.centerVButton.Text = "Center On Vertical Centerline";
			this.centerVButton.UseVisualStyleBackColor = true;
			this.centerVButton.Click += new System.EventHandler(this.centerVButton_Click);
			// 
			// centerHButton
			// 
			this.centerHButton.Location = new System.Drawing.Point(152, 41);
			this.centerHButton.Name = "centerHButton";
			this.centerHButton.Size = new System.Drawing.Size(168, 23);
			this.centerHButton.TabIndex = 2;
			this.centerHButton.Text = "Center On Horizontal Centerline";
			this.centerHButton.UseVisualStyleBackColor = true;
			this.centerHButton.Click += new System.EventHandler(this.centerHButton_Click);
			// 
			// MirrorVButton
			// 
			this.MirrorVButton.Location = new System.Drawing.Point(326, 12);
			this.MirrorVButton.Name = "MirrorVButton";
			this.MirrorVButton.Size = new System.Drawing.Size(168, 23);
			this.MirrorVButton.TabIndex = 3;
			this.MirrorVButton.Text = "Mirror On Vertical Centerline";
			this.MirrorVButton.UseVisualStyleBackColor = true;
			this.MirrorVButton.Click += new System.EventHandler(this.MirrorVButton_Click);
			// 
			// MirrorHButton
			// 
			this.MirrorHButton.Location = new System.Drawing.Point(326, 41);
			this.MirrorHButton.Name = "MirrorHButton";
			this.MirrorHButton.Size = new System.Drawing.Size(168, 23);
			this.MirrorHButton.TabIndex = 4;
			this.MirrorHButton.Text = "Mirror On Horizontal Centerline";
			this.MirrorHButton.UseVisualStyleBackColor = true;
			this.MirrorHButton.Click += new System.EventHandler(this.MirrorHButton_Click);
			// 
			// createCenterlinesButton
			// 
			this.createCenterlinesButton.Location = new System.Drawing.Point(12, 41);
			this.createCenterlinesButton.Name = "createCenterlinesButton";
			this.createCenterlinesButton.Size = new System.Drawing.Size(134, 23);
			this.createCenterlinesButton.TabIndex = 5;
			this.createCenterlinesButton.Text = "Create Centerlines";
			this.createCenterlinesButton.UseVisualStyleBackColor = true;
			this.createCenterlinesButton.Click += new System.EventHandler(this.createCenterlinesButton_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(506, 75);
			this.Controls.Add(this.createCenterlinesButton);
			this.Controls.Add(this.MirrorHButton);
			this.Controls.Add(this.MirrorVButton);
			this.Controls.Add(this.centerHButton);
			this.Controls.Add(this.centerVButton);
			this.Controls.Add(this.circlesButton);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button circlesButton;
		private System.Windows.Forms.Button centerVButton;
		private System.Windows.Forms.Button centerHButton;
		private System.Windows.Forms.Button MirrorVButton;
		private System.Windows.Forms.Button MirrorHButton;
		private System.Windows.Forms.Button createCenterlinesButton;
	}
}

