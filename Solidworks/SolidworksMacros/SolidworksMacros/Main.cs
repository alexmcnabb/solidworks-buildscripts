﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SldWorks;
using SwConst;
using static SolidworksMacros.Misc;
using static SolidworksMacros.SW;

namespace SolidworksMacros {


	class Main {
		public Main() {
			//StartOfTestingCode--
			
			//EndOfTestingCode--
		}

		public void createCenterlines() {
			setupSW();
			sw.skTools.getCenterLine(Direction.horizontal);
			sw.skTools.getCenterLine(Direction.vertical);
		}		

		public void createIdenticalCircles() {
			setupSW();
			CurrentSelection selection = new CurrentSelection(sw.swApp, sw.swDoc);

			if (selection.sketchPoints.Count == 0) {
				System.Windows.Forms.MessageBox.Show("No points selected");
				throw new DoneException();
			}
			double diameter = Input.getDouble("Circle Diameter");

			List<SketchSegment> circles = new List<SketchSegment>();
			foreach (SketchPoint centerPoint in selection.sketchPoints) {
				circles.Add(sw.skMan.CreateCircleByRadius(centerPoint.X, centerPoint.Y, centerPoint.Z, diameter / 2000));
			}
			sw.relMan.AddRelation((object)circles.ToArray(), (int)swConstraintType_e.swConstraintType_SAMELENGTH);
			SmartDimentioner sDim = new SmartDimentioner(sw.swApp, sw.swDoc);
			sDim.dimentionCircle(circles[0], diameter);
			sw.swDoc.GraphicsRedraw2();
		}

		internal void mirrorOn(Direction dir) {
			setupSW();
			CurrentSelection selection = new CurrentSelection(sw.swApp, sw.swDoc);
			SketchLine centerLine = sw.skTools.getCenterLine(dir);
			selection.resetSelection();
			selection.deselectObject(centerLine);
			foreach (object o in selection.allObjects) {
				selection.mark(o, 1);
			}
			selection.selectObject(centerLine);
			selection.mark(centerLine, 2);
			sw.swDoc.SketchMirror();

		}

		public void centerOn(Direction dir) {
			setupSW();
			CurrentSelection selection = new CurrentSelection(sw.swApp, sw.swDoc);
			SketchLine centerLine = sw.skTools.getCenterLine(dir);
			selection.sketchSegs.Remove((SketchSegment)centerLine);
			if (selection.sketchSegs.Count == 0) {
				System.Windows.Forms.MessageBox.Show("No entities selected");
				throw new DoneException();
			}
			foreach (SketchSegment skSeg in selection.sketchSegs) {
				sw.skTools.centerOnLine(skSeg, centerLine);
			}
		}


	}
}