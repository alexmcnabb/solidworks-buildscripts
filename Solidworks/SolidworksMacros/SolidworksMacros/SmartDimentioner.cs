﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SldWorks;
using SwConst;

namespace SolidworksMacros {
	class SmartDimentioner {
		SldWorks.SldWorks swApp;
		ModelDoc2 swDoc;

		public SmartDimentioner(SldWorks.SldWorks swApp, ModelDoc2 swDoc) {
			this.swApp = swApp;
			this.swDoc = swDoc;
		}
		public void dimentionCircle(SketchSegment circle, double diameter) {
			swApp.SetUserPreferenceToggle((int)swUserPreferenceToggle_e.swInputDimValOnCreate, false);
			circle.Select4(false, swDoc.SelectionManager.CreateSelectData());
			SketchPoint centerPoint = (SketchPoint)((SketchArc)circle).GetCenterPoint2();
			DisplayDimension dispDim = (DisplayDimension)swDoc.AddDiameterDimension2(centerPoint.X, centerPoint.Y, centerPoint.Z +  ((SketchArc)circle).GetRadius() * 1.1);
			swDoc.ClearSelection2(true);
			dispDim.GetDimension2(0).SystemValue = diameter / 1000.0;
			swApp.SetUserPreferenceToggle((int)swUserPreferenceToggle_e.swInputDimValOnCreate, false);
		}
	}
}