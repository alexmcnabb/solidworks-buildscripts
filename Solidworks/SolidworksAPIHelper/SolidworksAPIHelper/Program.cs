﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidworksAPIHelper {
	class Program {

		static bool debugMode = false;

		static void Main(string[] args) {
			if (args.Length == 0) {
				debugMode = true;
				Console.WriteLine("Debugmode On\n");
				args = new string[] { "exportDXF", "C:\\Users\\Alex\\Google Drive\\Laser Cutting\\1BuildScripts\\output\\svg", "False" };
			}

			Console.WriteLine("Starting SolidworksAPI Helper");
			string command = args[0];
			Console.WriteLine("Command = " + command);
			APIHelper apiHelper = new APIHelper();
			switch (command) {
				case "exportDXF":
					apiHelper.exportDXF(args);
					break;
				case "saveAs":
					apiHelper.saveAs(args);
					break;
				default:
					Console.WriteLine("Command not recognised");
					break;
			}

			Console.WriteLine("Done");
			if (debugMode) { Console.WriteLine("\nPress any key to continue"); Console.ReadKey(); }
		}

	}
}