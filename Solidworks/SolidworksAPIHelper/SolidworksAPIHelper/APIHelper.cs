﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SldWorks;
using SwConst;

namespace SolidworksAPIHelper {
	class APIHelper {
		public SldWorks.SldWorks swApp;
		public APIHelper() {
			swApp = new SldWorks.SldWorks();
		}


		public void exportDXF(string[] args) {
			string dest_folder = args[1] + "\\";
			bool currentonly = args[2] == "True";
			Console.WriteLine("Exporting DXF Files to " + dest_folder);
			Console.WriteLine(currentonly ? "Current configuration only" : "All subfiles");

			ExportDXF exporter = new ExportDXF(swApp, dest_folder, currentonly);
			exporter.exportActiveDoc();
		}


		public void saveAs(string[] args) {
			string dest_file = args[1];
			Console.WriteLine("Saving file to " + dest_file);

			ModelDoc2 swModel = (ModelDoc2)swApp.ActiveDoc;
			swModel.SaveAsSilent(dest_file, true);
		}
	}
}
