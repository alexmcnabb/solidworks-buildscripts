﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SldWorks;
using SwConst;

namespace SolidworksAPIHelper {
	class Util {


		public static bool isAssembly(ModelDoc2 swModel) {
			try {
				AssemblyDoc ass = (AssemblyDoc)swModel;
				return true;
			} catch (System.InvalidCastException) {
				return false;
			}
		}

		public static bool isDocumentOpen(SldWorks.SldWorks swApp, string modelName) {
			object[] docs = (object[])swApp.GetDocuments();
			foreach (object Odoc in docs) {
				ModelDoc2 doc = (ModelDoc2)Odoc;
				if (modelName == doc.GetPathName() && doc.Visible) return true;
			}
			return false;
		}

	}
}
