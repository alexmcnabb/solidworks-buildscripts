﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SldWorks;
using SwConst;
using static SolidworksAPIHelper.Util;

namespace SolidworksAPIHelper {
	class ExportDXF {
		private bool currentonly;
		private string dest_folder;
		private SldWorks.SldWorks swApp;

		private List<ModelDoc2> Done;

		public ExportDXF(SldWorks.SldWorks swApp, string dest_folder, bool currentonly) {
			this.dest_folder = dest_folder;
			this.currentonly = currentonly;
			this.swApp = swApp;
			Done = new List<ModelDoc2>();
		}

		public void exportActiveDoc() {
			ModelDoc2 swModel = (ModelDoc2)swApp.ActiveDoc;
			if (currentonly && isAssembly(swModel)) {
				Console.WriteLine("Can't export single configuration of assembly");
				return;
			}
			exportModelDocDXF(swModel);
		}


		void exportModelDocDXF(ModelDoc2 doc) {
			// Export either a part or assembly
			string name = doc.GetPathName().Split('\\').Last();
			if (Done.Contains(doc)) return; // skip any duplicates
			Done.Add(doc);

			if (isAssembly(doc)) {
				Console.WriteLine("Exporting assembly " + name);
				AssemblyDoc swAss = (AssemblyDoc)doc;
				object[] componentList = (object[])swAss.GetComponents(false);
				foreach (object Ocomponent in componentList) {
					Component2 component = (Component2)Ocomponent;
					ModelDoc2 subModel = component.GetModelDoc2();
					exportModelDocDXF(subModel);
				}
			} else {
				Console.WriteLine("Exporting part " + name);
				exportPartDXF(doc);
			}
		}

		void exportPartDXF(ModelDoc2 swModel) {
			string modelName = swModel.GetPathName();
			bool wasOpen = isDocumentOpen(swApp, modelName);
			object[] configNames = (object[])swModel.GetConfigurationNames();
			if (currentonly || configNames.Length == 1) {
				exportConfigurationDXF(swModel, "");
			} else {
				foreach (object OconfigName in configNames) {
					string configName = (string)OconfigName;
					Console.WriteLine("Exporting part configuration " + configName);
					swModel.ShowConfiguration2(configName);
					exportConfigurationDXF(swModel, configName);
				}
			}
			


			if (!wasOpen) swApp.CloseDoc(modelName); // Close document if was closed before
		}

		void exportConfigurationDXF(ModelDoc2 swModel, string configName) {
			string modelName = swModel.GetPathName();
			string suffix = configName == "" ? "" : "-" + configName;
			string filename = modelName.Substring(0, modelName.Length - 7) + suffix + ".dxf";
			filename = filename.Split('\\').Last();
			string pathName = dest_folder + filename;
			PartDoc swPart = (PartDoc)swModel;
			object varAlignment = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 };
			object varViews = new string[] { "*Top" };
			swPart.ExportToDWG2(pathName, modelName, (int)swExportToDWG_e.swExportToDWG_ExportAnnotationViews, true, varAlignment, false, false, 0, varViews);
		}
	}
}
