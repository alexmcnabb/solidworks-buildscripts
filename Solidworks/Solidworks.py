import os
import sys
import subprocess
from time import sleep

sys.path.append(os.path.join(__file__, "../"))

helperexe = os.path.abspath(os.path.join(__file__, "../SolidworksAPIHelper/SolidworksAPIHelper/bin/Debug/SolidworksAPIHelper.exe"))
macroexe = os.path.abspath(os.path.join(__file__, "../SolidworksMacros/SolidworksMacros/bin/Debug/SolidworksMacros.exe"))

class Solidworks:

	def __init__(self):
		pass

	def saveAs(self, filename):
		dest_file = os.path.abspath(filename)
		subprocess.call([helperexe, "saveAs", dest_file])

	def exportDXF(self, dest_folder, currentonly=False):
		dest_folder = os.path.abspath(dest_folder)
		subprocess.call([helperexe, "exportDXF", dest_folder, str(currentonly)])


def openMacros():
	subprocess.Popen([macroexe])

if __name__ == '__main__':
	print("starting")
	sw = Solidworks()
	sw.saveAs("testfile.stl")


