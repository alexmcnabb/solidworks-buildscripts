import sys
import os
from time import sleep
import subprocess
import autoit

root_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../"))
if root_path not in sys.path: sys.path.append(root_path)

import common.Autoit.Autoit as aut
from common.PixelConstructs import Point, Rect

adobe_exe = '"C:\Program Files (x86)\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe"'
propertiesWindowIdentifier = aut.window_identifier(cls="#32770", width=782)  # "[CLASS:#32770; W:782]"
# Title is blank, and class matches with the print dialog, so identify by width as well as class


this_path = os.path.dirname(__file__)

class AdobeReader:
	def __init__(self):
		self.window = aut.Window(" - Adobe Acrobat Reader DC", matchType=aut.MatchType.Substring)

	def winexists(self):
		return self.window.exists()

	def open(self):
		print("Opening Adobe Reader...", end="", flush=True)
		# Open Adobe Reader, and the file
		filepath = '{"' + os.path.abspath(os.path.join(root_path, "output/output.pdf")) + '"}'
		subprocess.Popen(["powershell.exe", "start", adobe_exe, filepath])
		self.window.waitTilExists()
		self.window.activate()
		print("Done")

	def closewindow(self):
		print("Closing Adobe Reader...", end="", flush=True)
		self.window.activate()
		propertiesWindow = aut.Window(propertiesWindowIdentifier)
		if propertiesWindow.exists():  # Need to close the print properties window specifically, if it exists
			propertiesWindow.activate()
			aut.send("{ESCAPE}")
			sleep(0.3)
		aut.send("{ESCAPE}")  # Close the print dialog
		sleep(0.3)
		self.window.close(kill=True)  # And close adobe reader
		print("Done")

	def print(self, profile=None):
		print("Printing document")
		print("Using profile " + profile)
		# If no profile passed, uses first one present
		self.window.activate()
		aut.send("^p")  # Open print dialog
		printDialog = aut.Window("Print", matchType=aut.MatchType.CompleteMatch)
		printDialog.waitTilExists()
		sleep(0.5)
		# Select correct printer
		printerDropdownBox = aut.Control(printDialog, cls="ComboBox", instance=1)
		for i in range(30): # Try at least for every printer
			selection = printerDropdownBox.getCurrentSelection()[0:4]
			# print(selection)
			if selection == "Epil":  # for some reason, only the first few letters are right, rest are all random data
				break
			printerDropdownBox.send("{DOWN}")   # TODO - make this work better...
			sleep(0.1)
		else:
			raise RuntimeError("Could not find printer name")
		# Check 'Actual Size' radio button
		printDialog.check_box(21).check()
		# Open properties window
		printDialog.button(4).click()
		propertiesWindow = aut.Window(propertiesWindowIdentifier)
		propertiesWindow.waitTilExists()
		sleep(0.2)
		# Click the Advanced button
		propertiesWindow.button(8).click()
		sleep(0.1)
		# Select the correct profile
		# If profile not given, use the first one
		if profile is not None:
			profileListControl = aut.ControlListView(propertiesWindow, cls="SysListView32", instance=1)
			index = profileListControl.indexOf(profile)
			if index == -1:
				raise RuntimeError("Profile not found")
			profileListControl.selectItemByIndex(index)
		# Load profile
		propertiesWindow.button(67).click()
		sleep(1)
		# Exit properties dialog
		propertiesWindow.button(2).click()
		sleep(2)
		# Print
		printDialog.button(50).click()
		print("Done")