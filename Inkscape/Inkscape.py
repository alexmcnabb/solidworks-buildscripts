import os
import sys
import subprocess
from time import sleep
from shutil import copy
import autoit


root_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "../"))
if root_path not in sys.path: sys.path.append(root_path)

from common.PixelConstructs import Point
import common.Autoit.Autoit as aut

inkscape_exe = '"C:\Program Files\Inkscape\inkscape.exe"'
inkscape_com = '"C:\Program Files\Inkscape\inkscape.com"'

this_path = os.path.dirname(__file__)


class Inkscape:
	def __init__(self):
		self.window = aut.Window(" - Inkscape", matchType=aut.MatchType.Substring)

	def winexists(self):
		return self.window.exists()

	def open(self):
		print("Opening Inkscape...", end="", flush=True)
		# Open inkscape, and the template file
		srcfile = os.path.join(this_path, "Template.svg")
		destfile = os.path.join(root_path, "output/output.svg")
		copy(srcfile, destfile)  # Move template to output folder
		template_path = '{"' + os.path.abspath(os.path.join(root_path, "output/output.svg")) + '"}'
		subprocess.Popen(["powershell.exe", "start", inkscape_exe, template_path])
		# Check this on laptop. If powershell isn't there, use this command instead
		# subprocess.Popen(inkscape_exe + " " + template_path, shell=True))
		self.window.waitTilExists()
		sleep(3)
		self.window.activate()
		print("Done")

	def cleardoc(self):
		print("Clearing Inkscape document")
		self.window.activate()
		aut.send("^a{DELETE}")

	def importfiles(self, folder):
		print("Importing files to Inkscape", end="", flush=True)
		self.window.activate()  # Not actually sure if this is necessary
		# Open file explorer in output/svg folder
		os.startfile(os.path.abspath(folder).replace("\\", "/"))  # For some stupid reason, this needs forward slashes
		# Wait for the window to open
		fileexplorer = aut.Window("svg", matchType=aut.MatchType.CompleteMatch)
		fileexplorer.waitTilExists()
		sleep(0.5)
		# Select all the files (Explorer window will be active here)
		fileexplorer.activate()
		aut.send("^a")
		# Find coords of file to click on
		selectionarea = aut.Control(fileexplorer, cls="DirectUIHWND", instance=3)
		startpoint = Point(40, 40) + selectionarea.absPosition().offsetaspoint()
		# Start the drag operation
		autoit.mouse_move(startpoint.x, startpoint.y)
		autoit.mouse_down("primary")
		autoit.mouse_move(startpoint.x, startpoint.y + 100)  # Trigger the files to attach to the mouse
		self.window.activate()
		# Prepare the destination window
		destpos = self.window.pos().center()
		autoit.mouse_move(destpos.x, destpos.y)
		# And deposit the files
		autoit.mouse_up("primary")
		print("Done")

	def saveaspdf(self):
		print("Saving Inkscape document as PDF...", end="", flush=True)
		self.window.activate()
		aut.send('^+s') # Open save-as window
		savewindow = aut.Window("Select file to save to", matchType=aut.MatchType.CompleteMatch)
		savewindow.waitTilExists()
		savewindow.activate()
		fileTypeCombo = aut.Control(savewindow, cls="ComboBox", instance=3)
		fileTypeCombo.select("Portable Document Format (*.pdf)")
		aut.send('{ENTER}')  # Exit Save As dialog
		sleep(1)
		aut.send('{ENTER}')  # Accept export options
		sleep(1)
		aut.send('{ENTER}')  # Force file over-write
		print("Done")


def ConvertToSVG(sourcefile, destfile):
	print("Converting {} to SVG".format(os.path.basename(sourcefile)))
	sourcefile = '"' + os.path.abspath(sourcefile).replace("\\", "/") + '"'
	destfile = '"' + os.path.abspath(destfile).replace("\\", "/") + '"'
	command = [inkscape_com, "--without-gui", sourcefile, "--export-area-drawing", "--export-plain-svg", destfile]
	commandstring = " ".join(command)
	print("Calling inkscape - Command = ", commandstring)
	popen = subprocess.Popen(commandstring, shell=True)  # Start inkscape
	exportWindow = aut.Window("DXF Input", matchType=aut.MatchType.CompleteMatch)
	exportWindow.waitTilExists()
	while exportWindow.exists():
		sleep(1)  # Wait for window to appear
		aut.send("{ENTER}")  # Close it
	popen.wait()  # Wait for inkscape to finish
	# TODO - Put a timeout on the wait() and get it to retry the enter if it missed it
	print("Done")


if __name__ == '__main__':
	print("starting")
	ConvertToSVG("C:\\Users\Alex\\Google Drive\\Laser Cutting\\1BuildScripts\\Stack-Brace.DXF", "C:\\Users\\Alex\Google Drive\\Laser Cutting\\1BuildScripts\\Stack-Brace.svg")


