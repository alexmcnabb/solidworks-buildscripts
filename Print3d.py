# Exports the active model from Solidworks, and loads it into Simplify3D

from time import sleep
import os

from Solidworks.Solidworks import Solidworks
import common.Autoit.Autoit as aut



def print3d():
	print("Exporting STL from Solidworks")
	sw = Solidworks()
	sw.saveAs("tmp.stl")

	print("Loading Model into Simplify3D")

	window = aut.Window("Simplify3D")
	window.activate()
	aut.send("^a{Delete}")  # Clear existing model
	aut.send("^i")  # Open import dialog
	aut.send(os.path.abspath("tmp.stl"))  # Type in file
	aut.send("{Enter}")  # Import it
	print("Done")