


class Point:
	def __init__(self, x, y):
		self.x = x
		self.y = y

	def __add__(self, other):
		return Point(self.x + other.x, self.y + other.y)

	def __radd__(self, other):
		if other == 0:
			return self
		else:
			return self.__add__(other)

	def __truediv__(self, other):
		return Point(self.x / other, self.y / other)


class Line:
	def __init__(self, a, b):
		self.a = a
		self.b = b


class Shape:
	def __init__(self, points):
		# points is list of all corners, ordered properly
		# last point connects to first point to close shape
		self.points = points

	@classmethod
	def make(cls, args):
		# Make a shape somehow
		# points = stuff(args)
		points = args
		return cls(points)

	def getCenter(self):
		return sum(self.points) / len(self.points)

	@property
	def lines(self):
		# build a list of the lines from the cornerpoints
		lines = []
		for i in range(0, len(self.points) - 1):
			lines.append(Line(self.points[i], self.points[i + 1]))
		lines.append(Line(self.points[-1], self.points[0]))
		return lines

	def contains(self, testshape):
		#Checks if the centerpoint of the testshape is inside
		testpoint = testshape.getCenter()
		# Find if a ray in the +x direction from the testpoint intersects edges
		# If it intersects an odd number of edges, the point is inside
		isInside = False
		for line in self.lines:
			isInside = isInside != self.intersect(testpoint, line)
		return isInside
		# See here for description of the edge cases, if they are an issue
		# http://marcodiiga.github.io/point-in-polygon-problem
		# http://geomalgorithms.com/a03-_inclusion.html

	# List of shapes contained inside this shape
	contents = []

	@staticmethod
	def intersect(testpoint, line):
		# Checks if a line in the positive x axis from testpoint
		# would intersect the given line
		crossesxaxis = line.a.x > testpoint.x != line.b.x > testpoint.x
		c = line.a.x - (line.b.x * line.a.y / line.b.y)
		d = 1 - (line.a.y / line.b.y)
		intercept = c / d
		return crossesxaxis == True and intercept > 0
