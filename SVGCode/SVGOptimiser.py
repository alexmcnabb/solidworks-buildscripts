import os

import xml.etree.ElementTree as ET


# from SVG.Elements import Point, Line, Shape



class SVGOptimiser:
	def __init__(self):
		self.tree = None
		self.grouproot = None
		self.filename = None

	def loadSVG(self, filename):
		self.filename = filename
		# Read in source file
		# Data is all in one group, the fourth element
		self.tree = ET.parse(os.path.abspath(filename))
		self.grouproot = self.tree.getroot()[3]
		# for child in self.grouproot:
		# 	print(child.tag, child.attrib)

	def fixLinewidth(self):
		for child in self.grouproot:
			child.attrib['style'] = child.attrib['style'].replace("stroke-width:0.9", "stroke-width:0.09")
		self.tree.write(self.filename)

if __name__ == "__main__":
	svgopt = SVGOptimiser()
	svgopt.loadSVG("C:/Users/Alex/Google Drive/Laser Cutting/1BuildScripts/Stack-Brace.svg")
	svgopt.fixLinewidth()



