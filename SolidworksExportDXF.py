from time import sleep
import os, shutil
import glob
import argparse

from Solidworks.Solidworks import Solidworks
from Inkscape.Inkscape import ConvertToSVG
from SVGCode.SVGOptimiser import SVGOptimiser

# This script exports the active document from solidworks
# Will open single parts, each configuration of a part, or each part in an assembly


def Main(currentonly=False):

	# Make sure the output folder is there
	os.makedirs("output/svg", exist_ok=True)

	# Export DXFs from solidworks, save to build folder
	sw = Solidworks()
	sw.exportDXF("output/svg", currentonly=currentonly)

	# Convert DXFs to SVG files using Inkscape
	print("Converting DXFs to SVGs")
	filelist = [os.path.abspath(f) for f in glob.glob("output/svg/*.dxf")]
	[ConvertToSVG(file, file.replace(".dxf", ".svg")) for file in filelist]
	print("Removing DXF files...", end="")
	for file in filelist:
		os.remove(file)
	filelist = [file.replace(".dxf", ".svg") for file in filelist]
	print("Done")

	# Optimise the SVG files for path order, line width
	print("Fixing line widths...", end="")
	svgopt = SVGOptimiser()
	for file in filelist:
		svgopt.loadSVG(file)
		svgopt.fixLinewidth()
	print("Done")

if __name__ == "__main__":
	Main()







