import sys
import os
import typing
import traceback
from time import sleep
from typing import List, Dict, Union, Callable
from functools import wraps
import readline

import cmd  # https://pymotw.com/2/cmd/index.html#module-cmd

def add_handling(func):
	@wraps(func)
	def wrapped_function(*argv, **kwargs):
		try:
			return func(*argv, **kwargs)
		except KeyboardInterrupt:
			print("\rAborted")  # Backspace over '^C' before printing
		except Exception:
			traceback.print_exc()
	return wrapped_function


def add_handling_to_class(cls):
	'''Apply add_handling decorator to all do_ functions'''
	for attr_name in dir(cls):
		attr_value = getattr(cls, attr_name)
		if hasattr(attr_value, '__call__'):
			if attr_value.__name__.startswith("do_"):
				setattr(cls, attr_name, add_handling(attr_value))
	return cls


class CommandProcessor(cmd.Cmd):
	def __init__(self):
		super().__init__()
		self.prompt = ">"

	def do_exit(self, line):
		'''Exit the program'''
		return True

	def emptyline(self):
		pass  # Default behaviour is to repeat the last command, disable that


def run_processor(cls):
	cls = add_handling_to_class(cls)
	processor = cls()
	processor.intro = processor.__doc__
	if len(sys.argv) > 1:
		processor.onecmd(' '.join(sys.argv[1:]))
	else:
		# x = True
		# while x:
		try:
			processor.cmdloop()
		except KeyboardInterrupt:
			sys.exit(0)




if __name__ == "__main__":
	# from common.InteractiveConsole.InteractiveConsole import CommandProcessor, run_processor

	class ExampleCommandProcessor(CommandProcessor):
		'''Example Command Processor'''

		def do_stuff(self, line):
			'''Does stuff'''
			print("Stuff")

	run_processor(ExampleCommandProcessor)