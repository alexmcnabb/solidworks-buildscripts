# from __future__ import Annotations
import sys
import os
import enum
import datetime
from time import sleep
from typing import Union, Dict, List
import typing
import autoit

common_root = os.path.abspath(os.path.join(os.path.dirname(__file__), "../"))
if common_root not in sys.path: sys.path.append(common_root)

from PixelConstructs import Point, Rect


def send(string):
	autoit.send(string)


class MatchType:  # TODO - check these, make Enum
	StartOfString = 1
	Substring = 2
	CompleteMatch = 3
	Regex = 4  # not sure about this one...

# To be used instead of the title by itself for identifing windows
def window_identifier(cls=None, width=None) -> str:
	elements = []
	if cls is not None:
		elements.append("CLASS:{}".format(cls))
	if width is not None:
		elements.append("W:{}".format(width))
	return "[" + "; ".join(elements) + "]"

class Window:
	def __init__(self, title: str, matchType: MatchType = MatchType.Substring):
		self.title = title
		self.matchType = matchType
		self.__handle = None

	def exists(self):
		autoit.auto_it_set_option("WinTitleMatchMode", self.matchType)
		return autoit.win_exists(self.title)

	@property
	def handle(self):
		if self.exists:
			if self.__handle is None:  # Get the handle the first time it's needed
				autoit.auto_it_set_option("WinTitleMatchMode", self.matchType)
				self.__handle = autoit.win_get_handle(self.title)
			return self.__handle
		else:
			raise RuntimeError("Retrieved window handle of non-existant window")

	def waitTilExists(self, timeout: float=None) -> bool:
		if timeout is not None:
			timestamp = datetime.datetime.now()
		while not self.exists():
			if timeout is not None and datetime.datetime.now - timestamp > timeout:  # TODO - Check units on this
				return False
			sleep(0.1)
		return True

	def activate(self):
		autoit.win_activate_by_handle(self.handle)

	def pos(self) -> Rect:  # not positive that this works....
		return Rect.fromautoit(autoit.win_get_pos_by_handle(self.handle))

	def close(self, kill: bool=False):  # Should we be using autoit.win_wait_close?
		if kill:
			autoit.win_kill_by_handle(self.handle)  # TODO - not sure if kill actually does anything different?
		else:
			autoit.win_close_by_handle(self.handle)
		self.__handle = None

	def clickByCoords(self, loc: Point):  # TODO - This function doesn't work yet, self.pos may be broken, not sure
		raise NotImplementedError
		print(self.pos())
		print(loc)
		pos = self.pos().offsetaspoint() + loc
		print(pos)
		autoit.mouse_click("left", pos.x, pos.y)
		# Windowposition - 569, 278
		# Windowsize - 782, 485
		# clickloc - 952, 323

	def button(self, instance):
		return Button(self, instance)

	def check_box(self, instance):
		return CheckBox(self, instance)

########################################################################################################################
class _ControlBase:
	def __init__(self, window: Window, cls: str=None, instance: int=None):
		elements = []
		if cls is not None:
			elements.append("CLASS:{}".format(cls))
		if instance is not None:
			elements.append("INSTANCE:{}".format(instance))
		self.IDstring = "[" + "; ".join(elements) + "]"
		self.window = window
		self.__handle = None

	@property
	def handle(self):
		if self.__handle is None:  # Get the handle the first time it's needed
			self.__handle = autoit.control_get_handle(self.window.handle, self.IDstring)
		return self.__handle

	def click(self):
		autoit.control_click_by_handle(self.window.handle, self.handle)

	def send(self, string: str):
		autoit.control_send_by_handle(self.window.handle, self.handle, string)

	def relativePosition(self) -> Rect:
		return Rect.fromautoit(autoit.control_get_pos_by_handle(self.window.handle, self.handle))

	def absPosition(self) -> Rect:
		return self.relativePosition().addoffset(self.window.pos().offsetaspoint())

class Control(_ControlBase):  # https://www.autoitscript.com/autoit3/docs/functions/ControlCommand.htm
	def command(self, command: str, extra: str=""):
		return autoit.control_command_by_handle(self.window.handle, self.handle, command, extra=extra)

	def getCurrentSelection(self) -> str:
		return self.command("GetCurrentSelection")

	def select(self, string: str):
		self.command("SelectString", extra=string)

class ControlListView(_ControlBase):  # https://www.autoitscript.com/autoit3/docs/functions/ControlListView.htm
	def command(self, command: str, extra: str="", extra2: str=""):
		return autoit.control_list_view_by_handle(self.window.handle, self.handle, command, extra1=extra, extra2=extra2)

	def getItemCount(self):
		return self.command("GetItemCount")

	def indexOf(self, string):
		# Returns -1 if not found
		return self.command("FindItem", extra=string)

	def selectItemByIndex(self, index: int):
		self.command("Select", extra=str(index))


########################################################################################################################
class Button:
	def __init__(self, window, instance):
		self.control = Control(window, cls="Button", instance=instance)

	def click(self):
		self.control.click()

class CheckBox:
	def __init__(self, window, instance):
		self.control = Control(window, cls="Button", instance=instance)

	def check(self):
		self.control.command("Check")

	def uncheck(self):
		self.control.command("UnCheck")

