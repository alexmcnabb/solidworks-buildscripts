from typing import Type, List, Dict, Union
import sys
try:
	from PyQt5.QtCore import QRect, QPointF
except ImportError:
	class QPointF:
		pass
	class QRect:
		pass


# Note - The rect class assumes that y increases downwards

class Point:
	def __init__(self, x: int, y: int):
		self.x = x
		self.y = y

	@classmethod
	def fromqpointf(cls, source: QPointF):
		return cls(int(source.x()), int(source.y()))

	def __add__(self, other: 'Point') -> 'Point':
		return Point(self.x + other.x, self.y + other.y)

	def __sub__(self, other: 'Point') -> 'Point':
		return Point(self.x - other.x, self.y - other.y)

	def __radd__(self, other: 'Point') -> 'Point':
		if other == 0:
			return self
		else:
			return self.__add__(other)

	def __floordiv__(self, other: Union[int, float]) -> 'Point':
		return Point(self.x // other, self.y // other)

	def __str__(self):
		return "Point( x={0}, y={1} )".format(self.x, self.y)


class Rect:
	def __init__(self, x: int, y: int, width: int, height: int):
		self.x = x
		self.y = y
		self.height = height
		self.width = width

	@classmethod
	def fromedges(cls, top: int, bottom: int, left: int, right: int) -> 'Rect':
		return cls(left, top, right - left, bottom - top)

	@classmethod
	def fromqt(cls, source: QRect) -> 'Rect':
		return cls(source.x(), source.y(), source.width(), source.height())

	@classmethod
	def fromautoit(cls, source: List[int]) -> 'Rect':
		return cls.fromedges(source[1], source[3], source[0], source[2])

	@property
	def top(self) -> int:
		return self.y

	@property
	def bottom(self) -> int:
		return self.y + self.height

	@property
	def left(self) -> int:
		return self.x

	@property
	def right(self) -> int:
		return self.x + self.width

	def shrink(self, sf: Union[int, float]) -> 'Rect':
		return Rect(self.x // sf, self.y // sf, self.width // sf, self.height // sf)

	def pad(self, padding: int) -> 'Rect':
		return Rect.fromedges(self.top - padding, self.bottom + padding, self.left - padding, self.right + padding)

	def center(self) -> Point:
		return Point((self.left + self.right) // 2, (self.top + self.bottom) // 2)

	def sizeaspoint(self) -> Point:
		return Point(self.width, self.height)

	def offsetaspoint(self) -> Point:
		return Point(self.x, self.y)

	def addoffset(self, offset: Point) -> 'Rect':
		return Rect(self.x + offset.x, self.y + offset.y, self.width, self.height)

	def isinside(self, point:Point) -> bool:
		return self.left < point.x < self.right and self.top < point.y < self.bottom

	@staticmethod
	def encloses(rects: List['Rect']) -> 'Rect':
		left = min([rect.left for rect in rects])
		right = max([rect.right for rect in rects])
		top = min([rect.top for rect in rects])
		bottom = max([rect.bottom for rect in rects])
		return Rect.fromedges(top, bottom, left, right)

	def __str__(self):
		return "Rectangle( x={0}, y={1}, width={2}, height={3} )".format(self.x, self.y, self.width, self.height)
