import sys
import os
import shutil
import glob
from time import sleep

import SolidworksExportDXF
import OpenInkscape
import LasercutFromInkscape
import Print3d
from Solidworks.Solidworks import openMacros
from common.InteractiveConsole.InteractiveConsole import CommandProcessor, run_processor

class BuildscriptCommandProcesor(CommandProcessor):
	"""Solidworks Buildscripts  Type 'help' for help, 'exit' to exit, ctrl-c to abort"""

	def do_exportall(self, line):
		"""Export active document with all children"""
		SolidworksExportDXF.Main(currentonly=False)

	def do_export(self, line):
		"""Export current configuration of active document"""
		SolidworksExportDXF.Main(currentonly=True)

	def do_openinkscape(self, line):
		"""Clear inkscape, and load all SVGs"""
		OpenInkscape.Main()

	def do_print(self, line):
		"""Print from inkscape"""
		LasercutFromInkscape.Main()

	def do_listsvg(self, line):
		"""List exported SVG files"""
		print("Exported SVG files -")
		files = glob.glob("output/svg/*.svg")
		if files:
			for file in glob.glob("output/svg/*.svg"):
				print("   ", end="")
				print(os.path.basename(file))
		else:
			print("    No files")

	def do_clearsvg(self, line):
		"""Clear exported SVG files"""
		if os.path.isdir("output/svg"):
			shutil.rmtree("output/svg")
		os.makedirs("output/svg")
	
	def do_3dprint(self, line):
		"""Export and print model with Simplify3D"""
		Print3d.print3d()
		
	def do_runmacros(self, line):
		"""Open the solidworks macros"""
		openMacros()

run_processor(BuildscriptCommandProcesor)

