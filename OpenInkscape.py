from time import sleep
import os
import shutil
import glob
import argparse

from Inkscape.Inkscape import Inkscape

# This script opens inkscape if it isn't already, clears and resizes the document
# and imports all the files in the build folder


def Main():
	# make the output folder
	os.makedirs("output", exist_ok=True)

	# Start up inkscape
	inkscape = Inkscape()
	if inkscape.winexists():
		print("Inkscape already open. Clearing document")
		inkscape.cleardoc()
	else:
		print("Opening Inkscape")
		inkscape.open()

	# Import all the files
	print("Importing Files")
	inkscape.importfiles("output/svg")





if __name__ == "__main__":
	Main()







