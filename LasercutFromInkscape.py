from time import sleep
import os, shutil
import glob
import argparse

from Inkscape.Inkscape import Inkscape
from AdobeReader.AdobeReader import AdobeReader

# This script sends the active document in Inkscape to the printer
# The print operation is done through Adobe Reader



def Main():
	adoreader = AdobeReader()
	if adoreader.winexists():
		adoreader.closewindow()

	inkscape = Inkscape()
	inkscape.saveaspdf()

	adoreader.open()

	adoreader.print(profile="Birch-0.125")


if __name__ == "__main__":
	Main()







